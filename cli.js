const inquirer = require('inquirer')
const logUpdate = require('log-update')
const Bot = require('./app')

const questions = [{
    type: 'input',
    name: 'owner',
    message: 'What\'s the owner?',
    validate(value) {
      if (!value) return 'Please enter owner'
      return true
    }
  },
  {
    type: 'input',
    name: 'repo',
    message: 'What\'s the repo?',
    validate(value) {
      if (!value) return 'Please enter repo'
      return true
    }
  }
]

inquirer.prompt(questions).then(answers => {
  Bot(answers.owner, answers.repo)
    .then(() => logUpdate('pr done.'))
    .catch(err => logUpdate(err))
})
