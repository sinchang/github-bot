const axios = require('axios')
const base64 = require('base-64')
const cheerio = require('cheerio')
const { token, githubId } = require('./config')

const BASE_URL = 'https://api.github.com'
const regex = /<a\starget='_blank'\srel='nofollow'\shref='https:\/\/app.codesponsor.io\/link(.*)\n(.*)\n<\/a>/g
const accessToken = `token ${token}`

function forkRepo(owner, githubId, repo) {
  return axios({
    method: 'POST',
    url: `${BASE_URL}/repos/${owner}/${repo}/forks`,
    headers: {
      Authorization: accessToken
    },
    data: {
      organization: githubId
    }
  }).then(res => Promise.resolve(res.data)).catch(err => Promise.reject(err))
}

function getReadmeContent(githubId, repo) {
  return axios({
    method: 'GET',
    url: `${BASE_URL}/repos/${githubId}/${repo}/readme`,
    headers: {
      Authorization: accessToken
    }
  }).then(res => Promise.resolve(res.data)).catch(err => Promise.reject(err))
}

function updateContent(githubId, repo, params = {}) {
  return axios({
    method: 'PUT',
    url: `${BASE_URL}/repos/${githubId}/${repo}/contents/${params.path}`,
    headers: {
      Authorization: accessToken
    },
    data: {
      ...params
    }
  }).then(res => Promise.resolve(res.data)).catch(err => Promise.reject(err))
}

function createPr(githubId, repo, params = {}) {
  return axios({
    method: 'POST',
    url: `${BASE_URL}/repos/${githubId}/${repo}/pulls`,
    headers: {
      Authorization: accessToken
    },
    data: {
      ...params
    }
  }).then(res => Promise.resolve(res.data)).catch(err => Promise.reject(err))
}

// function fetchRepo(page = 1) {
//   const url = `https://github.com/search?l=Markdown&p=${page}&q=href%3D%27https%3A%2F%2Fapp.codesponsor.io%2Flink&type=Code&utf8=%E2%9C%93&_pjax=%23js-pjax-container`
//   axios({
//     method: 'GET',
//     url,
//     headers: {
//       Authorization: accessToken
//     }
//   })
//     .then(res => {
//       // console.log(res.data)
//       const $ = cheerio.load(res.data)
//       const r = $('#code_search_results').html()
//       console.log(r)
//     })
// }
const Bot = (owner, repo) => {
  return forkRepo(owner, githubId, repo)
    .then((forkRes) => {
      const base = forkRes.default_branch
      return getReadmeContent(githubId, repo)
        .then(readmeRes => {
          const {
            path,
            sha,
            content
          } = readmeRes
          const text = base64.decode(content).replace(regex, '')
          return updateContent(githubId, repo, {
            path,
            message: 'remove codesponsor',
            sha,
            content: base64.encode(text)
          }).then(contentRes => {
            return createPr(owner, repo, {
              title: 'remove codesponsor',
              head: githubId + ':master',
              body: 'pull request by sinchang-bot <https://github.com/sinchang-bot>',
              base
            })
          })
        })
    })
}

module.exports = Bot
